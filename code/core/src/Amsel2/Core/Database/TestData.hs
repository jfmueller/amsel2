{-# LANGUAGE OverloadedStrings #-}
module Amsel2.Core.Database.TestData where

import           Amsel2.Core.Data.OrgEinheit
import qualified Amsel2.Core.Data.Adresse as A
import           Amsel2.Core.Data.Identifier
import           Amsel2.Core.Database.Versioning

{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
module Amsel2.Core.Database.Versioning where

import Amsel2.Core.Data.OrgEinheit
import Amsel2.Core.Data.Versioning

import           Control.Lens
import           Control.Monad.State         (get, put)
import           Data.Acid
import           Data.Hashable
import qualified Data.Map as M
import           Data.Maybe (listToMaybe)
import           Data.SafeCopy
import qualified Data.Set as S
import           Data.Table
import qualified Data.Text as T
import           Data.Typeable
import           GHC.Generics (Generic)

data Repository = Repository {
  _owner :: T.Text,
  _masterBranch :: Transaction,
  _branches :: M.Map T.Text Transaction,
  _tags :: M.Map T.Text Transaction
} deriving (Show, Generic, Typeable)

initialiseRepository :: T.Text -> Repository
initialiseRepository t = Repository t initialTransaction M.empty M.empty

emptyRepository :: Repository
emptyRepository = initialiseRepository ""

instance Hashable Repository where
  hashWithSalt s rp =
    s `hashWithSalt`
    _owner rp `hashWithSalt`
    _masterBranch rp `hashWithSalt`
    (hashMap $ _branches rp) `hashWithSalt`
    (hashMap $ _tags rp)

hashMap :: (Hashable k, Hashable v) => M.Map k v -> Int
hashMap = M.foldlWithKey (\a k b -> a `hashWithSalt` k `hashWithSalt` b) 23498713240987

instance Eq Repository where
  rp1 == rp2 = (hash rp1) == (hash rp2)
  rp1 /= rp2 = (hash rp1) /= (hash rp2)

$(deriveSafeCopy 0 'base ''Repository)

insertTransaction :: Transaction -> Update Repository Transaction
insertTransaction tr = do
  rp <- get
  let oldHead = _masterBranch rp
  let newHead = tr & parent .~ (Just oldHead)
  put $ rp{_masterBranch = newHead}
  return newHead

$(makeAcidic ''Repository ['insertTransaction])

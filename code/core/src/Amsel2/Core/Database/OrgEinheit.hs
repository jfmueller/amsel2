{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Amsel2.Core.Database.OrgEinheit where

import           Amsel2.Core.Data.OrgEinheit
import qualified Amsel2.Core.Data.Adresse as A
import           Amsel2.Core.Data.Identifier

import Control.Applicative hiding  (empty)
import Control.Exception           (bracket)
import Control.Monad.State         (get, put)
import Control.Monad.Reader        (ask)
import Data.Acid
import Data.Acid.Local             (createCheckpointAndClose)
import Data.SafeCopy
import Data.Table
import Data.Typeable

newtype OrgEinheitState = OrgEinheitState {
  _oeState :: Table OrgEinheit
} deriving (Typeable)

$(deriveSafeCopy 0 'base ''OrgEinheitState)

-- Insert an OrgEinheit into the table
-- Returns the same OrgEinheit with its new ID
insertOrgEinheit :: OrgEinheit -> Update OrgEinheitState OrgEinheit
insertOrgEinheit oe = do
  OrgEinheitState oes <- get
  let (oe', t') = insert' oe oes
  put $ OrgEinheitState t'
  return oe'

-- Get the OrgEinheit Table
getOrgEinheiten :: Query OrgEinheitState (Table OrgEinheit)
getOrgEinheiten = _oeState <$> ask

$(makeAcidic ''OrgEinheitState ['insertOrgEinheit, 'getOrgEinheiten])

initialOeState :: OrgEinheitState
initialOeState = OrgEinheitState $ singleton oe where
  oe = orgEinheit "Amsel GbR" "-" "001" $ A.adresse "Bornweg 15" "" "67157" "Wachenheim"

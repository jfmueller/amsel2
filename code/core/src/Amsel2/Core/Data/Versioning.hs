{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Amsel2.Core.Data.Versioning where

import           Data.Hashable
import qualified Data.Map as M
import           Data.SafeCopy
import qualified Data.Set as S
import           Data.Table
import qualified Data.Text as T
import           Data.Typeable
import           GHC.Generics (Generic)

import Amsel2.Core.Data.OrgEinheit

-- Change mode
-- Updates have the hash value of the old record (for consistency)
data ChangeMode = Insert | Update Int
  deriving (Eq, Ord, Show, Generic, Typeable)

instance Hashable ChangeMode

$(deriveSafeCopy 0 'base ''ChangeMode)

data RecordChange = UpdateOrgEinheit ChangeMode OrgEinheit
  deriving (Eq, Ord, Show, Generic, Typeable)

instance Hashable RecordChange

$(deriveSafeCopy 0 'base ''RecordChange)

data MergeStrategy = LastWins | FirstWins | AbortOnConflict
  deriving (Eq, Ord, Show)

data Transaction = Transaction {
  _author :: T.Text,
  _changes :: [RecordChange],
  _parent :: Maybe Transaction,
  _summary :: T.Text,
  _description :: T.Text,
  _hashValue :: Int
} deriving (Eq, Ord, Show, Generic, Typeable)

initialTransaction :: Transaction
initialTransaction = transaction T.empty [] Nothing T.empty T.empty

instance Hashable Transaction where
  hashWithSalt s t =
    s `hashWithSalt`
    _author t `hashWithSalt`
    _changes t `hashWithSalt`
    (fmap _hashValue $ _parent t) `hashWithSalt`
    _summary t `hashWithSalt`
    _description t

transaction :: T.Text -> [RecordChange] -> Maybe Transaction -> T.Text -> T.Text -> Transaction
transaction a c p s d = result where
  result = Transaction a c p s d $ hash result

-- lens for parent
parent :: Functor f => (Maybe Transaction -> f (Maybe Transaction)) -> Transaction -> f Transaction
parent f t@Transaction{..} = fmap (\parent' -> transaction _author _changes parent' _summary _description) (f _parent)

$(deriveSafeCopy 0 'base ''Transaction)

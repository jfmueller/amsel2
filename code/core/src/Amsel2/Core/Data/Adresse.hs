{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module Amsel2.Core.Data.Adresse(
  Adresse,
  adresse,
  strasse1,
  strasse2,
  plz,
  ort,
  adresseRules,
  adresseRecordDef
) where

import           Amsel2.Core.Rules.Records
import           Amsel2.Core.Rules.Rules
import           Data.Hashable
import           Data.SafeCopy
import qualified Data.Set as S
import qualified Data.Text as T
import           Data.Typeable
import           GHC.Generics (Generic)

-- | Adresse
data Adresse = Adresse {
  _strasse1 :: T.Text,
  _strasse2 :: T.Text,
  _plz      :: T.Text,
  _ort      :: T.Text
}
  deriving (Eq, Ord, Typeable, Show, Generic)

instance Hashable Adresse

$(deriveSafeCopy 0 'base ''Adresse)

-- Lens for Strasse1 component of Adresse
strasse1 :: Functor f => (T.Text -> f T.Text) -> Adresse -> f (Adresse)
strasse1 f ad = fmap (\s1' -> ad{_strasse1 = s1'}) (f $ _strasse1 ad)

-- Lens for Strasse2 component of Adresse
strasse2 :: Functor f => (T.Text -> f T.Text) -> Adresse -> f (Adresse)
strasse2 f ad = fmap (\s2' -> ad{_strasse2 = s2'}) (f $ _strasse2 ad)

-- Lens for PLZ component of Adresse
plz :: Functor f => (T.Text -> f T.Text) -> Adresse -> f (Adresse)
plz f ad = fmap (\plz' -> ad{_plz = plz'}) (f $ _plz ad)

-- Lens for Ort component of Adresse
ort :: Functor f => (T.Text -> f T.Text) -> Adresse -> f (Adresse)
ort f ad = fmap (\ort' -> ad{_ort = ort'}) (f $ _ort ad)

-- Constructor
adresse :: T.Text -> T.Text -> T.Text -> T.Text -> Adresse
adresse = Adresse

-- | Records

-- Record type for adresse
adresseRecordDef :: RecordDef
adresseRecordDef = RecordDef "adresse" $ Record $ S.fromList fields where
  fields = [s1, s2, p, o]
  s1 = RecordDef "strasse1" Text
  s2 = RecordDef "strasse2" Text
  p  = RecordDef "plz" Text
  o  = RecordDef "ort" Text

-- CSV read rules for adresse
adresseFromCsv :: Rule RecordDef
adresseFromCsv = Rule desc Pure precons adresseRecordDef where
  precons = fmap (flip RecordDef Text) ["strasse1", "strasse2", "plz", "ort"]
  desc = "Adresse lesen"

str1 :: Rule RecordDef
str1 = Rule desc (ReadCsvColumn False "strasse1") [] $ RecordDef "strasse1" Text where
  desc = "Strasse 1 von CSV lesen"

str2 :: Rule RecordDef
str2 = Rule desc (ReadCsvColumn True "strasse2") [] $ RecordDef "strasse2" Text where
  desc = "Strasse 2 von CSV lesen (optional)"

plzRule :: Rule RecordDef
plzRule = Rule desc (ReadCsvColumn True "plz") [] $ RecordDef "plz" Text where
  desc = "PLZ von CSV lesen (optional)"

ortRule :: Rule RecordDef
ortRule = Rule desc (ReadCsvColumn False "ort") [] $ RecordDef "ort" Text where
  desc = "Ort von CSV lesen"

adresseRules :: [Rule RecordDef]
adresseRules = [adresseFromCsv, str1, str2, plzRule, ortRule]

{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Amsel2.Core.Data.OrgEinheit(
  OrgEinheit,
  orgEinheit,
  name1,
  name2,
  betriebsnummer,
  adresse,
  orgEinheitId,
  orgEinheitRecordDef,
  orgEinheitRules
) where

import           Control.Applicative
import           Data.Hashable
import           Data.SafeCopy
import qualified Data.Set as S
import           Data.Table
import qualified Data.Text as T
import           Data.Typeable
import           GHC.Generics (Generic)

import           Amsel2.Core.Rules.Records
import           Amsel2.Core.Rules.Rules
import           Amsel2.Core.Data.Adresse hiding (adresse)
import           Amsel2.Core.Data.Identifier

-- | Organisationseinheit
data OrgEinheit = OrgEinheit {
  _orgEinheitId :: OrgEinheitId,
  _name1 :: T.Text,
  _name2 :: T.Text,
  _betriebsnummer :: T.Text,
  _adresse :: Adresse
}
  deriving (Eq, Ord, Typeable, Show, Generic)

instance Hashable OrgEinheit

$(deriveSafeCopy 0 'base ''OrgEinheit)

instance Tabular OrgEinheit where
  type PKT OrgEinheit = OrgEinheitId
  data Key k OrgEinheit b where
    OeId  :: Key Primary      OrgEinheit OrgEinheitId
    Name1 :: Key Supplemental OrgEinheit T.Text
    Name2 :: Key Supplemental OrgEinheit T.Text
    Betriebsnummer :: Key Supplemental OrgEinheit T.Text
  data Tab OrgEinheit i = OrgEinheitTab (i Primary OrgEinheitId) (i Supplemental T.Text) (i Supplemental T.Text) (i Supplemental T.Text)
  fetch OeId           = _orgEinheitId
  fetch Name1          = _name1
  fetch Name2          = _name2
  fetch Betriebsnummer = _betriebsnummer
  primary          = OeId
  primarily OeId r = r
  mkTab f = OrgEinheitTab <$> f OeId <*> f Name1 <*> f Name2 <*> f Betriebsnummer
  forTab (OrgEinheitTab i n1 n2 bn) f = OrgEinheitTab <$> f OeId i <*> f Name1 n1 <*> f Name2 n2 <*> f Betriebsnummer bn
  ixTab (OrgEinheitTab i _  _  _ ) OeId  = i
  ixTab (OrgEinheitTab _ n1 _  _ ) Name1 = n1
  ixTab (OrgEinheitTab _ _  n2 _ ) Name2 = n2
  ixTab (OrgEinheitTab _ _  _  bn) Betriebsnummer = bn
  autoTab = autoIncrement orgEinheitId

-- Lens for Name 1
name1 :: Functor f => (T.Text -> f T.Text) -> OrgEinheit -> f (OrgEinheit)
name1 f oe = fmap (\n1' -> oe{_name1 = n1'}) (f $ _name1 oe)

-- Lens for Name 2
name2 :: Functor f => (T.Text -> f T.Text) -> OrgEinheit -> f (OrgEinheit)
name2 f oe = fmap (\n2' -> oe{_name2 = n2'}) (f $ _name2 oe)

-- Lens for Betriebsnummer
betriebsnummer :: Functor f => (T.Text -> f T.Text) -> OrgEinheit -> f (OrgEinheit)
betriebsnummer f oe = fmap (\bn' -> oe{_betriebsnummer = bn'}) (f $ _betriebsnummer oe)

-- Lens for Adresse
adresse :: Functor f => (Adresse -> f Adresse) -> OrgEinheit -> f OrgEinheit
adresse f oe = fmap (\adr' -> oe{_adresse = adr'}) (f $ _adresse oe)

-- Lens for OrgEinheit-ID
orgEinheitId :: Functor f => (OrgEinheitId -> f OrgEinheitId) -> OrgEinheit -> f OrgEinheit
orgEinheitId f oe = fmap (\oeid' -> oe{_orgEinheitId = oeid'}) (f $ _orgEinheitId oe)

-- Create a new org-einheit with default ID
orgEinheit :: T.Text -> T.Text -> T.Text -> Adresse -> OrgEinheit
orgEinheit = OrgEinheit 0

-- | Records

-- record type for oe
orgEinheitRecordDef :: RecordDef
orgEinheitRecordDef = RecordDef "orgEinheit" $ Record $ S.fromList fields where
  fields = [n1, n2, nb, adresseRecordDef]
  n1  = RecordDef "name1" Text
  n2  = RecordDef "name2" Text
  nb  = RecordDef "betriebsnummer" Text

-- write OE to database
writeOeToDb :: Rule RecordDef
writeOeToDb = Rule desc (DbInsert OrgEinheitTbl) [orgEinheitRecordDef] nil where
  desc = "Org.Einheit in die Datenbank schreiben"

-- CSV read rules for Org-Einheiten
orgEinheitFromCsv :: Rule RecordDef
orgEinheitFromCsv = Rule desc Pure preconds orgEinheitRecordDef where
  preconds = adresseRecordDef : (fmap (flip RecordDef Text) ["name1", "name2", "betriebsnummer"])
  desc = "Org.Einheit lesen"

name1Rule :: Rule RecordDef
name1Rule = Rule desc (ReadCsvColumn False "name1") [] $ RecordDef "name1" Text where
  desc = "Name 1 von CSV lesen"

name2Rule :: Rule RecordDef
name2Rule = Rule desc (ReadCsvColumn True "name2") [] $ RecordDef "name2" Text where
  desc = "Name 2 von CSV lesen (optional)"

betriebsnummerRule :: Rule RecordDef
betriebsnummerRule = Rule desc (ReadCsvColumn False "betriebsnummer") [] $ RecordDef "betriebsnummer" Text where
  desc = "Betriebsnummer von CSV lesen"

orgEinheitRules :: [Rule RecordDef]
orgEinheitRules = [orgEinheitFromCsv, name1Rule, name2Rule, betriebsnummerRule, writeOeToDb]

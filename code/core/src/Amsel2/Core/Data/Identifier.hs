{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
-- An ID (Integer)
module Amsel2.Core.Data.Identifier(
  OrgEinheitId) where

import           Data.Hashable
import           Data.SafeCopy
import qualified Data.Text as T
import           Data.Typeable
import           GHC.Generics (Generic)

-- A unique identifier for OEs
newtype OrgEinheitId = OeId Integer
  deriving (Eq, Ord, Show, Typeable, Num, Generic)

instance Hashable OrgEinheitId

$(deriveSafeCopy 0 'base ''OrgEinheitId)

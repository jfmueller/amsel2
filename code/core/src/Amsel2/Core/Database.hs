module Amsel2.Core.Database where

import Amsel2.Core.Data.Versioning
import Amsel2.Core.Database.OrgEinheit (OrgEinheitState, initialOeState)
import Amsel2.Core.Database.Versioning

import Control.Exception           (bracket)
import Control.Monad.Reader        (ask)
import Data.Acid
import Data.Acid.Local             (createCheckpointAndClose)
import Data.SafeCopy
import qualified Data.Text as T

data AmselDb = AmselDb {
  _repository   :: AcidState Repository,
  _orgEinheiten :: AcidState OrgEinheitState
}

withAmselDb :: (AmselDb -> IO a) -> IO a
withAmselDb f =
  bracket (openLocalStateFrom ".amsel/gesundheitskartei/" initialOeState)  (createCheckpointAndClose) $ \orgEinheiten  ->
  bracket (openLocalStateFrom ".amsel/repository/" emptyRepository) (createCheckpointAndClose) $ \repo ->
  f (AmselDb repo orgEinheiten)

-- Apply a change to an amsel database (if possible)
applyChange :: MergeStrategy -> RecordChange -> AmselDb -> Either T.Text AmselDb
applyChange = undefined

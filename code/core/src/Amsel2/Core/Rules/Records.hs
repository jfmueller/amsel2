{-# LANGUAGE OverloadedStrings #-}
module Amsel2.Core.Rules.Records where

import qualified Data.Set as S
import qualified Data.Text as T

data RecordType = Number | Text | Bool | Record (S.Set RecordDef) | Nil
  deriving (Eq, Ord, Show)

data RecordDef = RecordDef T.Text RecordType
  deriving (Eq, Ord, Show)

recordDefName :: RecordDef -> T.Text
recordDefName (RecordDef n _) = n

nil :: RecordDef
nil = RecordDef "nil" Nil

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Amsel2.Core.Rules.Logic where

import Control.Applicative
import Control.Lens
import Control.Monad
import Control.Monad.Logic
import Control.Monad.Reader
import Data.Functor.Fixedpoint (Fix(..), anaM, cata)
import Data.Maybe (catMaybes, listToMaybe)
import qualified Data.Set as S

import Amsel2.Core.Data.OrgEinheit(orgEinheitRules, orgEinheitRecordDef)
import Amsel2.Core.Data.Adresse(adresseRules, adresseRecordDef)
import Amsel2.Core.Rules.Rules
import Amsel2.Core.Rules.Records

-- Beschreibung einer "Aktion" mit Nebeneffekten (d.h. in die Datenbank schreiben)
type ActionPlan = Fix Rule

-- Aktionenbauer
newtype Builder a = Builder { unBuilder :: ReaderT RuleSet Logic a }
  deriving (Functor, Applicative, Alternative, Monad, MonadReader RuleSet, MonadLogic, MonadPlus)

buildOne :: Builder a -> Maybe a
buildOne b = listToMaybe $ observeMany 1 $ runReaderT (unBuilder b) allRules

buildAll :: Builder a -> [a]
buildAll b = observeAll $ runReaderT (unBuilder b) allRules

-- Generate an action plan
plan :: RecordDef -> Builder ActionPlan
plan = anaM matchRules where
  matchRules fd = mfilter ((==) fd . view postcondition) rule

-- Generate an action plan with the given constraints
plan' :: S.Set Constraint -> RecordDef -> Builder ActionPlan
plan' cts = include . anaM matchRules  where
  matchRules fd = mfilter ((==) fd . view postcondition) $ exclude exs rule
  exs = exclusions cts
  incs = inclusions cts
  include = mfilter (isContainedIn incs . getRuleTypes)

-- Pick a rule from the knowledge base
rule :: Builder (Rule RecordDef)
rule = ask >>= msum . map return . getRules

-- Exclude a number of service types from the rule set
exclude :: S.Set RuleType -> Builder (Rule a) -> Builder (Rule a)
exclude ts = mfilter allowed where
  allowed = flip S.member ts . view ruleType

-- Alle Regeln, die im Amsel System zur Verfuegung stehen
allRules :: RuleSet
allRules = RuleSet (adresseRules ++ orgEinheitRules)

-- Restrictions on rules
data Constraint =
  Include RuleType
  | Exclude RuleType
  deriving (Eq, Ord, Show)

-- Get exclusions from a list of constraints
exclusions :: S.Set Constraint -> S.Set RuleType
exclusions = S.fromList . catMaybes . map getType . S.toList where
  getType (Include _) = Nothing
  getType (Exclude s) = Just s

-- Get inclusions from a list of constraints
inclusions :: S.Set Constraint -> S.Set RuleType
inclusions = S.fromList . catMaybes . map getType . S.toList where
  getType (Include s) = Just s
  getType (Exclude _) = Nothing

-- | Helper functions for querying action plans

-- Get the rule types involved in a plan
getRuleTypes :: ActionPlan -> [RuleType]
getRuleTypes = cata extract where
  extract = (:) <$> view ruleType <*> (concat . view precondition)

-- check if all elements of a list are contained in a set
isContainedIn :: (Ord a, Eq a) => S.Set a -> [a] -> Bool
isContainedIn theSet = flip S.isSubsetOf theSet . S.fromList

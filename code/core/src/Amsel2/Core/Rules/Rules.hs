{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
module Amsel2.Core.Rules.Rules where

import           Control.Applicative
import           Data.Foldable
import qualified Data.Set as S
import qualified Data.Text as T
import           Data.Traversable
import Prelude hiding (concat)

import Amsel2.Core.Rules.Records

type IsOptional = Bool

data DbTable = OrgEinheitTbl --TODO: Add other tables
  deriving (Eq, Ord, Show)

data RuleType =
  Pure
  | ReadCsvColumn IsOptional T.Text
  | DbInsert DbTable
  | DbUpdate DbTable
  deriving (Eq, Ord, Show)

data Rule a = Rule {
  _name :: T.Text,
  _ruleType :: RuleType,
  _precondition :: [a],
  _postcondition :: RecordDef
} deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

-- lens for rule name
name :: Functor f => (T.Text -> f T.Text) -> Rule a -> f (Rule a)
name f rl = fmap (\n' -> rl{_name = n'}) (f $ _name rl)

-- lens for rule type
ruleType :: Functor f => (RuleType -> f RuleType) -> Rule a -> f (Rule a)
ruleType f rl = fmap (\t' -> rl{_ruleType = t'}) (f $ _ruleType rl)

-- lens for precondition
precondition :: Functor f => ([a] -> f [a]) -> Rule a -> f (Rule a)
precondition f rl = fmap (\p' -> rl{_precondition = p'}) (f $ _precondition rl)

-- lens for postcondition
postcondition :: Functor f => (RecordDef -> f RecordDef) -> Rule a -> f (Rule a)
postcondition f rl = fmap (\p' -> rl{_postcondition = p'}) (f $ _postcondition rl)

newtype RuleSet = RuleSet { getRules :: [Rule RecordDef] }

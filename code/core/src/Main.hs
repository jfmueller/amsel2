-- http server for amsel
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Amsel2.Core.Database

import Web.Apiary
import Network.Wai.Handler.Warp
import qualified Data.ByteString.Lazy.Char8 as L

main :: IO ()
main = runApiary (run 3000) def $ do
  [capture|/age::Int|] . ([key|name|] =: pLazyByteString) . method GET . action $ do
      (age, name) <- [params|age,name|]
      guard (age >= 18)
      contentType "text/html"
      mapM_ appendLazyBytes ["<h1>Hello, ", name, "!</h1>\n"]

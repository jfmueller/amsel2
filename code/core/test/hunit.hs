{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
-----------------------------------------------------------------------------
-- |
-- Module      :  Main (hunit)
-- Copyright   :  (C) 2015 Jann Mueller
-- License     :  BSD-style (see the file LICENSE)
-- Maintainer  :  Jann Mueller <j.mueller.11@ucl.ac.uk>
-- Stability   :  provisional
-- Portability :  portable
--
-- A handful of unit tests for the core library.
--
-----------------------------------------------------------------------------
module Main where

import Amsel2.Core.Parser.LensLang (expr)
import Amsel2.Core.Language.LensLang
import qualified Data.Map  as M
import qualified Data.Set  as S
import qualified Data.Text as T
import Text.Parsec
import Text.Parsec.Text
import Test.Framework.Providers.HUnit
import Test.Framework.TH
import Test.Framework
import Test.HUnit hiding (test)

writeRecord :: String -> T.Text -> Exp
writeRecord rec val = EApp (EApp (EPrim $ RecordExtend rec) (EPrim $ Text val)) (EPrim RecordEmpty)

case_parse_app =
  let ex = parse expr "test" (T.pack "{name2=_|_} \"Amsel GbR\" {}") in
  let res = Right $ writeRecord "name2" "Amsel GbR" in
  ex @?= res

case_parse_prim =
  let actual   = parse expr "test" (T.pack "{name1=_|_}") in
  let expected = Right $ (EPrim $ RecordExtend "name1") in
  actual @?= expected

case_parse_read_oe =
  let actual   = parse expr "test" (T.pack "@[ORG]@") in
  let expected = Right $ (EPrim $ PRead $ Resource $ T.pack "ORG") in
  actual @?= expected

case_ti_read_oe =
  let (actual, _) = runTI $ typeInference M.empty $ EPrim $ PRead $ Resource $ T.pack "ORG" in
  let expected = Right $ TEffect (readRes $ Resource $ T.pack "ORG") TFinal in
  actual @?= expected

case_typecheck_write_record =
  let expr = writeRecord "name1" "OE1" in
  let (actual, _) = runTI $ typeInference M.empty $ expr in
  let expected = Right $ TRecord $ TRowExtend "name1" TText TRowEmpty in
  actual @?= expected

case_typecheck_write_resource =
  let expr = (EPrim $ PWrite $ Resource $ T.pack "ORG") in
  let (actual, _) = runTI $ typeInference M.empty $ expr in
  let resType = TRecord $ TRowExtend "name1" TText TRowEmpty in
  let expected = Right $ TFun resType $ TEffect (writeRes $ Resource $ T.pack "ORG") TFinal in
  actual @?= expected

case_typecheck_app_write =
  let expr = EApp (EPrim $ PWrite $ Resource $ T.pack "ORG") (EApp (EApp (EPrim $ RecordExtend "name1") (EPrim $ Text $ T.pack "OE1")) (EPrim RecordEmpty)) in
  let (actual, _) = runTI $ typeInference M.empty expr in
  let expected = Right $ TEffect  (writeRes $ Resource $ T.pack "ORG") TFinal in
  actual @?= expected

case_typecheck_bind = -- TODO: Write test that reads and writes, the result should contain both effects
  let expr = EBind m f in
  let m = 

case_unify_write_resource =
  let record = TRecord $ TRowExtend "name1" TText TRowEmpty in
  let left = TFun record $ TEffect (writeRes $ Resource $ T.pack "ORG") TFinal in
  let right = TFun record $ TVar $ TyVar "a4" Star S.empty in
  let (result, _) = runTI $ unify left right in
  let actual = case result of
        (Right _) -> True
        (Left _)  -> False in
  actual @?= True

main :: IO ()
main = defaultMain [$testGroupGenerator]

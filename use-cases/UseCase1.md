ich habe die von der Assistentin durchgeführten Untersuchungen eingetragen: also bei mehreren Probanden folgendes gemacht:

Sehtest G25, Perimetrie  und G20.1 zu einem in der Vergangenheit liegenden Datum angemeldet, dann direkt die Beurteilung gemacht mit Auswahl des nächsten Untersuchungsdatums aus der Auswahlliste. Manchmal mit Kommentar für der AG oder AN auf der Bescheinigung.

Das ist das einzige, was ich in amsel machen kann. Die inhaltliche Dokumentation findet im avis statt, da sich der Befundtext im amsel nicht abspeichert.

Ich bräuchte bei der Auswahl des Zeitpunkts für die nächste Untersuchung noch eine Möglichkeit  „4Monate“;  in der Größenordnung spielen sich alle kurzfristigen Wiedereinbestellungen ab.

Als zweites bräuchte ich die Möglichkeit, Vorfälle für die Zukunft einzuplanen .Zum Beispiel, wenn ich sehe, dass jemand zwar Sehteste bekommt, aber keine G25 beim Arzt vorgesehen ist. Ich könnte dann einfach für die Zukunft einen Vorfall anlegen, den die Personalabteilung dann automatisch mit aufruft.

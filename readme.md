# amsel2
Software for health services

## TO DO:
1. Fix repl-server.cmd (make it compile)
1. HTTP endpoint for query using rules
1. Implement branching/versioning
1. Build action plan for importing OrgEinheiten in CSV
1. Two HTTP endpoints: query, runAction
1. Web UI for csv import
1. Web UI for branches & transaction history


- Make OrgEinheiten queryable on CLI
  1. implement type check for (get l r) OK
  2. add evaluator
  3. Think about modularity (adding data types)
  4. Tuples and/or named records?
- Make OEs hierarchical
- Import OEs from CSV
- Web UI for OEsa

## Done
1. hashing
1. map string -> branch
1. Save branch/version info in acid state db

## Ideas
1. For field-based read/write permissions, add a functor to each field, ie.

'''
data Proband a = Proband {
  _name :: a T.Text,
  _name2 :: a T.Text
  ...
}
'''

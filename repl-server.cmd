@echo off

set DOCKER_CERT_PATH=C:\Users\User\.boot2docker\certs\boot2docker-vm
set DOCKER_TLS_VERIFY=1
set DOCKER_HOST=tcp://192.168.59.103:2376

docker run -v /c/Users/User/projects/amsel2/code:/opt/amsel2 -w /opt/amsel2 -it -p 3000:3000 jmueller/amsel2-dev
